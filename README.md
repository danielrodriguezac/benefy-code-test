# Benefy Code Test

## Backend

### Pre-requisites

Nodejs 12+ is required, a MySQL server

### Run Mysql on docker

The parameters seen here are the ones expected in the backend, they can also be modified at `benefy-backend/config/config.json`

sudo docker run --name=mysql-test \
-e MYSQL_ROOT_PASSWORD="NotSoTerriblePassword" \
-e MYSQL_DATABASE="benefy-test" \
-e MYSQL_USER="benefy" \
-e MYSQL_PASSWORD="benefy" \
-p 3306:3306 -p 33060:33060 -d mysql/mysql-server:latest

Some mysql environments might require this parameter after `mysql/mysql-server:latest` due to driver inconsistencies:

    --default-authentication-plugin=mysql_native_password
    # instead of
    --default-authentication-plugin=caching_sha2_password

You can peep into the server using this command

    # As benefy
    sudo docker exec -it mysql-test mysql -ubenefy -pbenefy
    # As root
    sudo docker exec -it mysql-test mysql -uroot -pNotSoTerriblePassword

If you have a running instance you can create a new database and a user with all priviledges granted on it, please verify these statements before running them on your server

    CREATE DATABASE benefy-test;
    CREATE USER 'benefy'@'localhost' IDENTIFIED BY 'benefy';
    GRANT ALL PRIVILEGES ON benefy-test . * TO 'benefy'@'localhost';
    FLUSH PRIVILEGES;

### Express + GraphQL server

Install dependencies and run migrations/seeding

    cd benefy-backend
    npm install
    npx sequelize-cli db:migrate && npx sequelize-cli db:seed:all

Start the server

    # NODE_ENV=production will keep stdout tidier, if not included SQL queries will be printed out
    NODE_ENV=production npm start

After initialization the GraphQL schema will be available at: `http://localhost:3000/`. Test it on the console:

    curl \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{ "query": "{ users{ id userName firstName lastName email createdAt updatedAt } }" }' \
    http://localhost:3000/

You can undo the database migration/seeding:

    npx sequelize-cli db:migrate:undo

## Frontend
    WIP
