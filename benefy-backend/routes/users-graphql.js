let graphqlHTTP = require('express-graphql');
let { buildSchema } = require('graphql');

const db = require('../models');

// Construct a schema, using GraphQL schema language
let schema = buildSchema(`
    input UserInput {
        userName: String
        firstName: String
        lastName: String
        email: String
    }
    type User {
        id: ID
        userName: String
        firstName: String
        lastName: String
        email: String
        createdAt: String
        updatedAt: String
    }
    type Query {
        user(id: ID): User
        users: [User]
    }
    type Mutation {
        createUser(user: UserInput): User
        updateUser(id: ID!, user: UserInput!): Int
        deleteUser(id: ID!): Boolean
    }
`);

// The root provides a resolver function for each API endpoint
let root = {
    user: async ({ id }) => {
        const userList = await db.Users.findAll({
            where: {
                id
            }
        });
        if (userList.length === 0) {
            throw new Error(`User with id: ${id} was not found in the database`);
        }
        // console.log(userList);
        return userList[0];
    },
    users: async () => {
        const userList = await db.Users.findAll({});
        // console.log(userList);
        return userList;
    },
    createUser: async ({ user }) => {
        const createdUser = await db.Users.create(user);
        return createdUser;
    },
    updateUser: async ({ id, user }) => {
        const updateObject = {
            updatedAt: new Date().toISOString()
        };
        for (const property in user) {
            if (user[property] != null) {
                if (user[property] != null) {
                    updateObject[property] = user[property];
                }
            }
        }
        const affectedCount = await db.Users.update(updateObject, {
            where: {
                id
            }
        });
        return +affectedCount;
    },
    deleteUser: async ({ id }) => {
        const deletedUser = await db.Users.destroy({
            where: {
                id
            }
        });
        // console.log(deletedUser);
        return deletedUser;
    }
};

module.exports = graphqlHTTP({
    schema: schema,
    rootValue: root
});