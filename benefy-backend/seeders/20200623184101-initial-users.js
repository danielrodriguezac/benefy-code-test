'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return queryInterface.bulkInsert('Users', [
      {
        userName: "benefy",
        firstName: 'Audacious',
        lastName: 'Squirrel',
        email: 'drodriguez@benefy.cl', // ;)
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userName: "Shiftie",
        firstName: 'Carabella',
        lastName: 'Stride',
        email: 'sorento@benefy.cl',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return queryInterface.bulkDelete('Users', null, {});
  }
};
